"use strict";

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    addSrc = require('gulp-add-src'),
    del = require('del'),
    browserSync = require("browser-sync").create(),
    plumber = require('gulp-plumber'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    cssMinify = require('gulp-clean-css'),
    mq = require('gulp-combine-mq'),
    fixJs = require("gulp-fixmyjs"),
    packer = require('gulp-uglify'),
    svgSprite = require("gulp-svg-sprite"),
    hb = require('gulp-hb');


var paths = {
    stage:  'stage',
    build:  'build',
    source: 'src',
    html:   'src/html/',
    style:  'src/less/main.less',
    images: 'src/img/**/*',
    ico:    'src/ico/*.svg',
    fonts:  'src/fonts',
    jq:     'bower_components/jquery/dist/'
};

var watch = {
    html:   'src/html/**/*',
    style:  'src/less/**/*',
    js:     'src/js/**/*',
    images: 'src/img/**/*',
    ico:    'src/ico/**/*'
};

// Js variables
var jsLoad = [
    'bower_components/svg-sprite-injector/svg-sprite-injector.js',
    'bower_components/enquire/dist/enquire.js',
    paths.source + '/js/load.js'
];

var jsVendor = [
    //'bower_components/jquery/dist/jquery.js',
    'bower_components/moment/moment.js',
    'bower_components/fullcalendar/dist/fullcalendar.js',
    'bower_components/owl.carousel/dist/owl.carousel.js',
    'bower_components/jquery-selectric/public/jquery.selectric.js',
    'bower_components/jquery-selectric/public/plugins/jquery.selectric.placeholder.js',
    'bower_components/jquery-ui/jquery-ui.js',
    'bower_components/jquery-ui/ui/widgets/datepicker.js',
    'bower_components/jquery-ui/ui/datepicker.js',
    'bower_components/jquery-ui/ui/i18n/datepicker-ru.js',
    'bower_components/jquery-ui/ui/widgets/slider.js',
    'bower_components/jquery-ui/ui/slider.js',
    'bower_components/fotorama/fotorama.js',
    'bower_components/jquery.scrollbar/jquery.scrollbar.js',
    'bower_components/jquery.maskedinput/dist/jquery.maskedinput.js',
    'bower_components/chartist/dist/chartist.js',
    'bower_components/webui-popover/dist/jquery.webui-popover.js',
    'bower_components/fullcalendar/dist/lang/ru.js',
    'bower_components/bootstrap/dist/js/bootstrap.js',
	  'bower_components/jquery.rateit/scripts/jquery.rateit.js'
];

var jsMain = [
    paths.source + '/js/main.js'
]

gulp.task('build', [
  'build-html',
  'build-css',
  'build-fonts',
  'jq-copy',
  'build-js',
  'build-img',
  'build-ico'
]);

// DEL
gulp.task('clean', function () {
    del.sync(paths.build);
});

// Handlebars HTML
gulp.task('build-html', function () {
    var hbStream = hb()
        // Partials
        .partials(paths.html + 'partials/*.hbs')
        // Helpers
        .helpers(require('handlebars-helpers'))
  return gulp
        .src(paths.html + 'pages/*.hbs')
        .pipe(plumber())
        .pipe(hbStream)
        .pipe(rename({extname: '.html'}))
        .pipe(gulp.dest(paths.build));
});


// Less build
gulp.task('build-css', function(){
  return gulp.src(paths.style)
        .pipe(plumber())
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(mq({
            beautify: true
        }))
        //.pipe(cssMinify())
        //.pipe(rename('main.min.css'))
        .pipe(gulp.dest(paths.build + '/css'));
});


// JS build
gulp.task('jq-copy', function(){
  return gulp.src(paths.jq + 'jquery.min.js')
        .pipe(gulp.dest(paths.build + '/js'));
});

gulp.task('build-js-load', function() {
  return gulp.src(jsLoad)
  .pipe(plumber())
  .pipe(addSrc.prepend(paths.build +'/js/temp/*.js'))
  .pipe(concat('load.js'))
  .pipe(fixJs())
  .pipe(packer())
  .pipe(rename('load.min.js'))
  .pipe(gulp.dest(paths.build +'/js'));
  del([
    paths.build +'/js/temp/'
  ])
});

gulp.task('build-main-js', function() {
  return gulp.src(jsMain)
        .pipe(plumber())
        .pipe(fixJs())
        .pipe(concat('main.js'))
        //.pipe(packer())
        //.pipe(rename('main.min.js'))
        .pipe(gulp.dest(paths.build +'/js'));
});

gulp.task('build-js', ['build-js-load', 'build-main-js'], function() {
  return gulp.src(jsVendor)
        .pipe(plumber())
        .pipe(fixJs())
        .pipe(concat('vendor.js'))
        .pipe(packer())
        .pipe(rename('vendor.min.js'))
        .pipe(gulp.dest(paths.build +'/js'));
});


// Img build
gulp.task('build-img', function(){
  return gulp.src(paths.images + '.{jpg,png,svg,ico,gif}')
        .pipe(gulp.dest(paths.build + '/img'));
});


// Ico-build
gulp.task('build-ico', function () {
  return gulp.src(paths.ico)
        .pipe(plumber())
        .pipe(svgSprite({
            mode: {
                symbol: true
            },
            shape: {
                transform: [{
                    svgo: {
                        plugins: [
                            {convertShapeToPath: true},
                            {convertPathData: true},
                            {mergePaths: true},
                            {convertTransform: true},
                            {removeUnusedNS: true},
                            {cleanupIDs: true},
                            {cleanupNumericValues: true},
                            {removeUselessStrokeAndFill: true},
                            {removeHiddenElems: true},
                            {removeDoctype: false}
                        ]
                    }
                }]
            }
        }))
        .pipe(rename('ico/sprite.svg'))
        .pipe(gulp.dest(paths.build));
});


gulp.task('build-fonts', function(){
  return gulp.src(paths.fonts + '/**/*')
        .pipe(gulp.dest(paths.build + '/fonts'));
});

//Watch
gulp.task('html-watch', ['build-html'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('css-watch', ['build-css'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('js-watch', ['build-js'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('img-watch', ['build-img'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('ico-watch', ['build-ico'], function (done) {
    browserSync.reload();
    done();
});

// browser-sync
gulp.task('default', ['build-html', 'build-css', 'build-fonts', 'jq-copy', 'build-js', 'build-img', 'build-ico'], function () {

  browserSync.init({
    port: 9000,
    // open: true,
    // notify: true,
    // browser: 'default',
    reloadDelay: 3000,
    // minify: false,
    server: {
      baseDir: paths.build,
	  index: "main.html"
    }
  });

  gulp.watch(watch.html, ['html-watch']);
  gulp.watch(watch.style, ['css-watch']);
  gulp.watch(watch.js, ['js-watch']);
  gulp.watch(watch.images, ['img-watch']);
  gulp.watch(watch.ico, ['ico-watch']);

});
