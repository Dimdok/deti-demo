$(document).ready(function(){
    // Carousel
    var baseClass           = 'carousel',
        loadingClass        = 'owl-loading',
        loadedClass         = baseClass + '--loaded',
        contentClass        = baseClass + '__content',
        contentWrapClass    = contentClass + 'Wrap',
        dragClass           = baseClass + '--drag',
        grabClass           = baseClass + '--grab',
        itemClass           = baseClass + '__item',
        navClass            = baseClass + '__nav',
        navItemClass        = navClass  + '-item',
        navItemsClass       = [navItemClass + ' ' + navItemClass + '--prev', navItemClass + ' ' + navItemClass + '--next'],
        dotListClass        = baseClass + '__dotList',
        dotListItemClass    = dotListClass + '-item';

    enquire.register("screen and (min-width: 460px)", {
        match : function() {
            $('.partnerList').owlCarousel({
                // Base classes
                loadingClass        : loadingClass,
                loadedClass         : loadedClass,
                stageClass          : contentClass,
                stageOuterClass     : contentWrapClass,
                dragClass           : dragClass,
                grabClass           : grabClass,
                itemClass           : itemClass,
                navContainerClass   : navClass,
                navClass            : navItemsClass,
                dotsClass           : dotListClass,
                dotClass            : dotListItemClass,

                // Base settings
                margin: 0,
                nav: false,
                items: 3,

                // Responsive settings
                responsive:{
                    460: {
                        items: 3
                    },
                    640: {
                        items: 4
                    },
                    880: {
                        items: 6
                    }
                }
            });

        },
        unmatch : function() {
            $('.partnerList').trigger('destroy.owl.carousel');
        }
    });

    $('.campSlider').owlCarousel({
        // Base classes
        loadingClass        : loadingClass,
        loadedClass         : loadedClass,
        stageClass          : contentClass,
        stageOuterClass     : contentWrapClass,
        dragClass           : dragClass,
        grabClass           : grabClass,
        itemClass           : itemClass,
        navContainerClass   : navClass,
        navClass            : navItemsClass,
        dotsClass           : dotListClass,
        dotClass            : dotListItemClass,

        // Base settings
        autoplay: true, //PAUSE TILL WORK IN PROGRESS
        margin: 0,
        nav: false,
        items: 1,
        loop: true,
        mouseDrag: false,
        touchDrag: true,
        pullDrag: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,

        // Responsive settings
        responsive:{
            460: {
                //items: 3
            },
            640: {
                //items: 4
            },
            880: {
                //items: 6
            }
        }
    });

    // var campSliderItemsLenht = $('.campSlider').find(".carousel__item").not($(".cloned")).length;
    //
    // if (campSliderItemsLenht > 1) {
    //     var setIntervalId = setInterval(function() {
    //         $('.campSlider').trigger('next.owl.carousel',[250]);
    //         // console.log("next");
    //     }, 4000);
    //
    //     $('.campSlider').on("mouseenter", function() {
    //        clearInterval(setIntervalId);
    //        console.log("over");
    //     });
    //
    //     $('.campSlider').on("mouseleave", function() {
    //        var setIntervalId = setInterval(function() {
    //             $('.campSlider').trigger('next.owl.carousel',[250]);
    //         // console.log("next");
    //         }, 4000);
    //        console.log("leave");
    //     })
    // }

// camp options carousel

    $('.campSliderDecr').owlCarousel({

        // Base classes
        loadingClass        : loadingClass,
        loadedClass         : loadedClass,
        stageClass          : contentClass,
        stageOuterClass     : contentWrapClass,
        dragClass           : dragClass,
        grabClass           : grabClass,
        itemClass           : itemClass,
        navContainerClass   : navClass,
        navClass            : navItemsClass,
        dotsClass           : dotListClass,
        dotClass            : dotListItemClass,

        // Base settings
        margin: 4,
        nav: true,
        items: $('.campSliderDecr').data("items"),
        autoWidth: true,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        loop: false,
        autoplay: false,
        // autoplaySpeed: true,
        // autoplayTimeout: 1000,
        dots: false,
        onInitialized: function(event) {
            var
                $this = $(event.target),
                navItemsLeft = $this.find('.carousel__nav-item--prev').empty().html(createMarkup("left", "carousel__nav-item--prev-icon")),
                navItemsRight = $this.find('.carousel__nav-item--next').empty().html(createMarkup("right", "carousel__nav-item--next-icon"));

            function createMarkup(direction, className) {
                var markup = '<i data-ico="arrow-' + direction + '-small" class="ico ' + className +'">'
                            + '<svg class="ico__svg">'
                                + '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-' + direction + '-small"></use>'
                            + '</svg>'
                            + '</i>';

                return markup;
            }
        },

        // Responsive settings
        responsive:{
            460: {
                //items: 3
            },
            640: {
                //items: 4
            },
            880: {
                //items: 6
            }
        }
    });

    $('select').selectric();

    $("input[type='tel']").mask("+7 (999) 999-99-99");

    // $('select').on('selectric-open', function(element){

    //   var
    //     outer = $(this).closest('.selectric-wrapper').find(".selectric-scroll"),
    //     inner = outer.find("ul"),
    //     options = {
    //         trackWidth: 10,
    //         barMinHeight: 10,
    //         offsetTop: 0,
    //         offsetBottom: 0,
    //     };

    //     if (outer.height() < inner.height()) {
    //         outer.addClass('scrollbar-rail');
    //         outer.scrollbar();
    //     }

    // });

    $('.scrollbar-rail').scrollbar();

// распорядок дня программы
    $('.programm-schedule__slider').owlCarousel({

        // Base classes
        loadingClass        : loadingClass,
        loadedClass         : loadedClass,
        stageClass          : contentClass,
        stageOuterClass     : contentWrapClass,
        dragClass           : dragClass,
        grabClass           : grabClass,
        itemClass           : itemClass,
        navContainerClass   : navClass,
        navClass            : navItemsClass,
        dotsClass           : dotListClass,
        dotClass            : dotListItemClass,

        // Base settings
        margin: 0,
        nav: true,
        items: $('.programm-schedule__slider').data('items'),
        autoWidth: true,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        loop: true,
        autoplay: false,
        // autoplaySpeed: true,
        // autoplayTimeout: 1000,
        dots: false,
        onInitialized: function(event) {
            var
                $this = $(event.target),
                navItemsLeft = $this.find('.carousel__nav-item--prev').empty().html(createMarkup("left", "carousel__nav-item--prev-icon")),
                navItemsRight = $this.find('.carousel__nav-item--next').empty().html(createMarkup("right", "carousel__nav-item--next-icon"));

            function createMarkup(direction, className) {
                var markup = '<i data-ico="arrow-' + direction + '-small" class="ico ' + className +'">'
                            + '<svg class="ico__svg">'
                                + '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-' + direction + '-small"></use>'
                            + '</svg>'
                            + '</i>';

                return markup;
            }
        },

        // Responsive settings
        responsive:{
            460: {
                //items: 3
            },
            640: {
                //items: 4
            },
            880: {
                //items: 6
            }
        }
    });
});

// main nav
$(document).ready(function(){
    if (!$(".nav__item").length) {return false;}

    var itemClass = 'nav__item',
        linkClass = itemClass+'-link',
        activeClass = itemClass+'--active';

    $('.'+linkClass).on('click', function(e){
        var $link = $(this),
            $item = $link.closest('.'+itemClass);

        if($link.hasClass(linkClass+'--sub')) {
            e.preventDefault();
            if($item.hasClass(activeClass)) {
                $item.removeClass(activeClass);
            } else {
                $item.addClass(activeClass).siblings().removeClass(activeClass);
            }
        }
    });
});

// datepicker
$(document).ready(function() {

    if ($(".datepicker__input").length) {


        $.datepicker.setDefaults({
          onClose: function(e) {
                $(this).parent().find(".datepicker__icon--open").hide();
                $(this).parent().find(".datepicker__icon--close").show();
          },
          beforeShow: function(e) {
                $(this).parent().find(".datepicker__icon--open").show();
                $(this).parent().find(".datepicker__icon--close").hide();
          }
        });

        var datepicker = $(".datepicker__input");

        $.each(datepicker, function(i, elem) {
            $(elem).datepicker( $.datepicker.regional[ "ru" ] );
        })

        $(".datepicker__icon--close").on("click", function(e) {
            e.preventDefault();
            $(this).parent().find(".datepicker__input").datepicker( "show" );
        });

        $(".datepicker__icon--open").on("click", function(e) {
            e.preventDefault();
            $(this).parent().find(".datepicker__input").datepicker( "hide" );
        });

    }
});

//slider-range
$(document).ready(function() {

    if ($(".slider-range__inner").length) {

        $(".slider-range__inner").each(function (i, slider) {
            var
                $this = $(this),
                maxRange =  $this.data("max-range"),
                minRange =  $this.data("min-range"),
                minVal =  $this.data("min-val"),
                maxVal =  $this.data("max-val"),
                minInput =  $this.closest(".slider-range-block").find(".slider-range-input--min"),
                maxInput =  $this.closest(".slider-range-block").find(".slider-range-input--max"),
                options = {
                    range: true,
                    animate: 600,
                    max: maxRange,
                    min: minRange,
                    values: [ minVal, maxVal ],
                    slide: function (event, ui) {
                        minInput.val(ui.values[0]);
                        maxInput.val(ui.values[1]);
                    }
                }

            $this.slider(options);

            minInput.val($this.slider("values", 0));
            maxInput.val($this.slider("values", 1));
        });

    }
});

// camp card & block-info
$(document).ready(function() {
    if($('.camp-card__tab').length) {

        $('.camp-card__tab').each(function(){
            var $tab = $(this),
                $btn = $tab.find('.camp-card__tab-title');
            $btn.on('click', function(){
                $tab.toggleClass('open');
            });
        });
    }

    if($(".block-info__title-link").length) {

        $(".block-info").each(function(){
            var
                $block = $(this),
                link = $block.find(".block-info__title-link");

            link.on("click", function(e) {
                e.preventDefault();

                $block.toggleClass('open');
            });
        });
    }
});

// tabs
$(document).ready(function() {

    if ($(".tabs").length) {

        $(".tabs").each(function(){
            var
                $this = $(this),
                tabsItem = $this.find(".tabs__link"),
                tabs = $this.find(".tabs__tab");

            tabsItem.on("click", function(e) {
                e.preventDefault();

                var
                    $this = $(this),
                    href = $this.attr("href");

                $this.parent().siblings().removeClass('tabs__item--active');
                $this.parent().addClass('tabs__item--active');

                tabs.each(function() {
                    var $this = $(this);

                    if ($this.attr("id") == href.slice(1)) {
                        $this.addClass('tabs__tab--active ')
                    } else {
                        $this.removeClass('tabs__tab--active ')
                    }
                });
            });

        });

    }
});

// Camp Tabs
$(document).ready(function() {

    if ($('.camp-card__fullinfo').length) {

        $('.camp-card__fullinfo').each(function(){

            var $wrap = $(this),
                $tabs = $wrap.find('.camp-card__fullinfo-button'),
                $cont = $wrap.find('.camp-card__fullinfo-content');

            $tabs.on('click', function(){
                $tabs.removeClass('camp-card__fullinfo-button--active');
                $(this).addClass('camp-card__fullinfo-button--active');
                $cont.removeClass('camp-card__fullinfo-content--active');
                $($(this).attr('href')).addClass('camp-card__fullinfo-content--active');

                return false;

            });

        });
    }
});

// Programm Tabs
$(document).ready(function() {

    if ($('.programm-tabs').length) {

        $('.programm-tabs').each(function(){

            var $wrap = $(this),
                $tabs = $wrap.find('.programm-tabs__button'),
                $cont = $wrap.find('.programm-tabs__content');

            $tabs.on('click', function(){
                $tabs.removeClass('programm-tabs__button--active');
                $(this).addClass('programm-tabs__button--active');
                $cont.removeClass('programm-tabs__content--active');
                $($(this).attr('href')).addClass('programm-tabs__content--active');

                return false;

            });

        });
    }
});

// fotorama
$(document).ready(function() {

    if ($('.fotorama').length) {

        var
            $fotorama = $('.fotorama'),
            $fotoramaDiv = $fotorama.fotorama(),
            fotorama = $fotoramaDiv.data('fotorama'),
            fullscreenTriger = $fotorama.parent().find(".slider-gallery__fullscreen-triger"),
            fotoramaNativeFullscreenTriger = $fotorama.find(".fotorama__fullscreen-icon"),
            thrumbs = $fotorama.find(".fotorama__nav");

        fullscreenTriger.on("click", function(e) {
            e.preventDefault();

            fotorama.requestFullScreen();
            fotoramaNativeFullscreenTriger.addClass('fotorama__fullscreen-icon--visible');
            thrumbs.addClass('fotorama__nav--align-center ');
        });

        $fotorama.on('fotorama:fullscreenexit', function (e, fotorama, extra) {
            fotoramaNativeFullscreenTriger.removeClass('fotorama__fullscreen-icon--visible');
            thrumbs.removeClass('fotorama__nav--align-center ');
        });
    }
});

// main-filter
$(document).ready(function() {

    if ($(".main-filter").length) {

      var
        mainFilter = $(".main-filter"),
        toggle = mainFilter.find(".main-filter__toggle-link"),
        extra = mainFilter.find(".main-filter__inner--extra"),
        // icon = extra.parent().find("svg").children(),
        icon = toggle.find("svg").children(),
        text = toggle.find("span");
    // camp card


        toggle.on("click", function(e) {
          e.preventDefault();

          var $this = $(this);

          if ($this.hasClass('open')) {
            $this.removeClass('open').addClass('close');
            extra.slideUp(100);
            icon.attr("xlink:href", "#plus");
            $this.find("span").text('Показать доп. фильтры')
          } else {
            $this.removeClass('close').addClass('open');
            extra.slideDown(100);
            icon.attr("xlink:href", "#minus");
            $this.find("span").text('Скрыть доп. фильтры')
          }
        });
    }
});

// add child block
$(document).ready(function() {

    if ($(".order-child__link").length) {

        var
            $childToggle = $(".order-child__title");
            telInput = $("input[type='tel']").mask("+7 (999) 999-99-99");

        $childToggle.on("click", function(e) {
            e.preventDefault();

            var
                $this = $(this),
                outer = $this.closest(".order-child");

            if (outer.hasClass('order-child--active')) {
                outer.removeClass('order-child--active');
                $this.find("svg").children().attr("xlink:href", $this.attr("data-open-icon"));
            } else {
                outer.addClass("order-child--active");
                $this.find("svg").children().attr("xlink:href", $this.attr("data-close-icon"));
            }
        });
    }
});

//search map
$(document).ready(function() {
    if ($('.search-map').length) {
        var
            map = $(".search-map"),
            mapToggle = $('[href="#map-tab"]');

        // mapToggle.on("click", _getData);

        mapToggle.on("click", function(e) {
            e.preventDefault();

            $.ajax({
                url: "camp.json",
                dataType: "json"
            }).done(function(data) {
                // console.log(JSON.stringify(data));
                var data = jQuery.parseJSON(JSON.stringify(data));

                _createGeoObjects(data);
            });
        });

        // function _getData(e) {
        //     e.preventDefault();

        //     $.ajax({
        //         url: "camp.json"
        //     }).done(function(data) {
        //         _createGeoObjects(data);
        //     });
        // }

        function _createGeoObjects(data) {
            var
                geoObject1 = [],
                geoObject2 = [],
                geoObject3 = [],
                geoObject4 = [],
                geoObject5 = [];

            for (var i = 0; i < data.length; i++) {
                if (data[i].typecamp == "city") {
                    geoObject1.push(data[i].coordinates);
                } else if (data[i].typecamp == "country") {
                    geoObject2.push(data[i].coordinates);
                } else if (data[i].typecamp == "nature") {
                    geoObject3.push(data[i].coordinates);
                } else if (data[i].typecamp == "sea") {
                    geoObject4.push(data[i].coordinates);
                } else if (data[i].typecamp == "abroad") {
                    geoObject5.push(data[i].coordinates);
                }
            }

            _crateMap(geoObject1, geoObject2, geoObject3, geoObject4, geoObject5);
        }

        // создает карту
        function _crateMap(geoObject1, geoObject2, geoObject3, geoObject4, geoObject5) {
            var
                myMap,
                mapDestroy = $('[href="#list-tab"]');

            map.find("ymaps").remove();

            ymaps.ready(init);

            function init() {

                myMap = new ymaps.Map("search-map", {
                    center: [49.58588340196902,40.88602722657522],
                    zoom: 4,
                    controls: []
                }),
                    coords1 = geoObject1,
                    coords2 = geoObject2,
                    coords3 = geoObject3,
                    coords4 = geoObject4,
                    coords5 = geoObject5,
                    city = new ymaps.GeoObjectCollection({}, {
                        iconLayout: "default#image",
                        iconImageClipRect: [[0, 65], [77, 135]],
                        iconImageHref: "img/map-pins.png",
                        iconImageSize: [77, 65],
                        iconImageOffset: [-27, -60]
                    }),
                    country = new ymaps.GeoObjectCollection({}, {
                        iconLayout: "default#image",
                        iconImageClipRect: [[0, 135], [77, 201]],
                        iconImageHref: "img/map-pins.png",
                        iconImageSize: [77, 65],
                        iconImageOffset: [-27, -60]
                    }),
                    nature = new ymaps.GeoObjectCollection({}, {
                        iconLayout: "default#image",
                        iconImageClipRect: [[0, 201], [77, 269]],
                        iconImageHref: "img/map-pins.png",
                        iconImageSize: [77, 65],
                        iconImageOffset: [-27, -60]
                    }),
                    sea = new ymaps.GeoObjectCollection({}, {
                        iconLayout: "default#image",
                        iconImageClipRect: [[0, 269], [77, 335]],
                        iconImageHref: "img/map-pins.png",
                        iconImageSize: [77, 65],
                        iconImageOffset: [-27, -60]
                    }),
                    abroad = new ymaps.GeoObjectCollection({}, {
                        iconLayout: "default#image",
                        iconImageClipRect: [[0,0], [77, 65]],
                        iconImageHref: "img/map-pins.png",
                        iconImageSize: [77, 65],
                        iconImageOffset: [-27, -60]
                    });

                for (var i = 0; i < coords1.length; i++) {
                    city.add(new ymaps.Placemark(coords1[i]));
                }

                for (var i = 0; i < coords2.length; i++) {
                    country.add(new ymaps.Placemark(coords2[i]));
                }

                for (var i = 0; i < coords3.length; i++) {
                    nature.add(new ymaps.Placemark(coords3[i]));
                }

                for (var i = 0; i < coords4.length; i++) {
                    sea.add(new ymaps.Placemark(coords4[i]));
                }

                for (var i = 0; i < coords5.length; i++) {
                    abroad.add(new ymaps.Placemark(coords5[i]));
                }

                myMap.geoObjects.add(city);
                myMap.geoObjects.add(country);
                myMap.geoObjects.add(nature);
                myMap.geoObjects.add(sea);
                myMap.geoObjects.add(abroad);
            }

            mapDestroy.on("click", function(e) {
                myMap.destroy();
            });
        }
    }
});

// Круговая диаграмма
(function(window, document, Chartist) {
  'use strict';

  var defaultOptions = {
    sliceMargin: 4
  };

  Chartist.plugins = Chartist.plugins || {};
  Chartist.plugins.ctSliceDonutMargin = function(options) {

    options = Chartist.extend({}, defaultOptions, options);

    return function ctSliceDonutMargin(chart) {
      if(chart instanceof Chartist.Pie) {
          chart.on('draw', function(data) {
            if(data.type === 'slice') {

                if (Math.abs(data.startAngle - data.endAngle) < options.sliceMargin)
                    {
                    return;
                    }

                  var start = Chartist.polarToCartesian(data.center.x, data.center.y, data.radius, data.startAngle + options.sliceMargin/2),
                  end = Chartist.polarToCartesian(data.center.x, data.center.y, data.radius, data.endAngle - options.sliceMargin/2),
                  arcSweep = data.endAngle - data.startAngle <= 180 ? '0' : '1',
                  d = [
                    // Start at the end point from the cartesian coordinates
                    'M', end.x, end.y,
                    // Draw arc
                    'A', data.radius, data.radius, 0, arcSweep, 0, start.x, start.y
                  ];

                // Create the SVG path
                var path = new Chartist.Svg('path', {
                  d: d.join(' ')
                }, data.element._node.className.baseVal);

                // Adding the pie series value to the path
                path.attr({
                  'value': data.value
                }); //Chartist.xmlNs.uri);

                // If this is a donut, we add the stroke-width as style attribute
                  path.attr({
                    'style': 'stroke-width: ' + data.element._node.style.strokeWidth
                  });
                data.element.replace(path);
            }
          });
      }
    };
  };

}(window, document, Chartist));

$(document).ready(function() {
    if ($(".chart").length) {

        var chart = $(".chart"), //структура программы - диаграмма
            diagram = chart.find(".chart__diagram-item"),
            num = chart.find(".chart__caption b"),
            data =[],
            dataScience = diagram.attr("data-chart-science"),
            dataMasterclass = diagram.attr("data-chart-masterclass"),
            dataEvenining = diagram.attr("data-chart-evenining"),
            dataActive = diagram.attr("data-chart-active"),
            count = 0,
            last = 0,
            percetn = 0,
            dataEnd = [];

            data.push(+dataScience,+dataMasterclass,+dataEvenining,+dataActive);

            for (var i = 0; i < data.length; i++) {
                count += data[i];
            }

            num.html(count);

            for (var j = 0; j < data.length; j++) {
                dataEnd.push(Math.floor(data[j] / count * 100));
                percetn += Math.floor(data[j] / count * 100);
            }

            if (percetn !== 100) {
                for (var i = 0; i < dataEnd.length - 1; i++) {
                    last += dataEnd[i];
                }

                last = 100 - last;

                dataEnd.pop();
                dataEnd.push(last);
            }

            //console.log(dataEnd);

        new Chartist.Pie('.ct-chart', {
            series: dataEnd
        }, {
            donut: true,
            donutWidth: 20,
            startAngle: 150,
            total: 100,
            showLabel: false,
            plugins: [
                Chartist.plugins.ctSliceDonutMargin({
                    sliceMargin: 20
                })
            ]
        });
    }
});

// Скрол для внутреннего меню
$(document).ready(function() {
    if ($('.camp-card__nav-item').length) {
        var links = $('.camp-card__nav-item');

        links.on('click', function(e) {
          e.preventDefault();
          var $this = $(this), hash = this.href.replace(/[^#]*(.*)/, '$1'),
            // id элемента, к которому нужно перейти
            targetSection = $(hash), scrollTime = 600;
          //console.log($this, hash, targetSection);
          $('body,html').animate({ 'scrollTop': targetSection.offset().top - 20 }, scrollTime);
        });
    }
});

// Календарь
$(document).ready(function() {

    if ($(".calendar").length) {

        var
            calendar = $(".calendar"),
            calendarItem = calendar.find(".calendar__main"),
            toolBar = calendar.find(".fc-toolbar"),
            firstLink = calendar.find(".calendar__tabs-link--first"),
            secondtLink = calendar.find(".calendar__tabs-link--second"),
            options = {
                eventLimit: 3,
                eventLimitClick: "popover",
                fixedWeekCount: false,
                aspectRatio: 2.05,
                customButtons: {
                    titleNextYear: {
                        text: "",
                    },
                    fakeButton: {
                        //
                    },
                    jan: {
                        text: "Январь",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-01-01T01:32:21.196+0600");
                        }
                    },
                    feb: {
                        text: "Февраль",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-02-01T01:32:21.196+0600");
                        }
                    },
                    mar: {
                        text: "Март",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-03-01T01:32:21.196+0600" );
                        }
                    },
                    apr: {
                        text: "Апрель",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-04-01T01:32:21.196+0600" );
                        }
                    },
                    may: {
                        text: "Май",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-05-01T01:32:21.196+0600" );
                        }
                    },
                    jun: {
                        text: "Июнь",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-06-01T01:32:21.196+0600" );
                        }
                    },
                    jul: {
                        text: "Июль",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-07-01T01:32:21.196+0600" );
                        }
                    },
                    agu: {
                        text: "Август",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-08-01T01:32:21.196+0600" );
                        }
                    },
                    sep: {
                        text: "Сентябрь",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-09-01T01:32:21.196+0600" );
                        }
                    },
                    okt: {
                        text: "Октябрь",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-10-01T01:32:21.196+0600" );
                        }
                    },
                    nov: {
                        text: "Ноябрь",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-11-01T01:32:21.196+0600" );
                        }
                    },
                    dec: {
                        text: "Декабрь",
                        click: function() {
                            calendarItem.fullCalendar( 'gotoDate', calendarItem.fullCalendar('getDate')._i[0] + "-12-01T01:32:21.196+0600" );
                        }
                    }
                },
                header: {
                    left:   'prevYear title',
                    center: 'jan feb mar apr may jun jul agu sep okt nov dec',
                    right:  'titleNextYear nextYear'
                },
                titleFormat: "YYYY",
                lang: "ru",
                columnFormat: "dddd",
                eventLimitText: "...",
                dayClick: function(date, jsEvent, view) {
                    // $(this).popover({
                    //     title: 'haha',
                    //     placement: 'right',
                    //     content: 'haha',
                    //     html: true,
                    //     container: 'body'
                    // });
                    // $(this).popover('show');
                    // alert('Clicked on: ' + date.format());

                    // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                    // alert('Current view: ' + view.name);

                    // // change the day's background color just for fun
                    // $(this).css('background-color', 'red');
                    // console.log(this);
                },
                selectable: true,
                events: {
                    url: '/events1.json',
                    error: function() {
                        console.log('there was an error while fetching events!');
                    },
                    success: function(doc) {

                    },
                    color: "transparent",
                    textColor: '#778899' // a non-ajax option
                },
                // eventClick: function(event, element) {

                //     event.title = "CLICKED!";

                //     $('#calendar').fullCalendar('updateEvent', event);

                // },
                viewRender: function( view, element ) {
                    var
                    currentDate = calendarItem.fullCalendar('getDate')._i,
                    currentMonth = currentDate[1],
                    currentYear = currentDate[0],
                    buttonsMonths = $('.fc-center .fc-button');

                  $('.fc-toolbar').find('.fc-titleNextYear-button').text(currentYear + 1);

                  // console.log(currentMonth.split(" ")[1]);
                  // console.log(currentMonth);

                  buttonsMonths.each(function (i, elem) {
                    $(elem).removeClass('active');
                  });

                  $(buttonsMonths[currentMonth]).addClass('active');

                },
                eventRender: function(event, element) {
                    element.html(""
                        + "<div class='fc-content'>"
                        +   "<span class='fc-title'>" + ((event.title != undefined) ? event.title : "") + "</span> "
                        +   "<span class='fc-desc-custom'>" + ((event.description != undefined) ? event.description : "") + "</span>"
                        + "</div>"
                    );
                },
                eventAfterAllRender: function( view ) {
                    if (!$('.fc-more-cell').length) {return false;}
                    var
                        cell = $('.fc-more-cell'),
                        row = $('.fc-more-cell').closest(".fc-row");
                        row.css({"height": "auto"});

                    $.each($('.fc-more-cell'), function (i, item) {
                        var $item = $(item);
                        // $item.prev().removeClass('fc-limited--visible');
                        $item.appendTo($item.prev());
                        $item.parent().addClass('fc-limited--visible');
                    });


                },
                handleWindowResize: false
            };


        calendarItem.fullCalendar(options);

        secondtLink.on("click", function(e) {
            e.preventDefault();

            var $this = $(this);

            if ($this.hasClass('active')) {
                return;
            }

            $this.parent().find(".calendar__tabs-link").removeClass('active');
            $this.addClass('active');
            calendarItem.fullCalendar( 'removeEventSource', '/events1.json' );
            calendarItem.fullCalendar( 'addEventSource', '/events2.json' );
        });

        firstLink.on("click", function(e) {
            e.preventDefault();

            var $this = $(this);

            if ($this.hasClass('active')) {
                return;
            }

            $this.parent().find(".calendar__tabs-link").removeClass('active');
            $this.addClass('active');

            calendarItem.fullCalendar( 'removeEventSource', '/events2.json' );
            calendarItem.fullCalendar( 'addEventSource', '/events1.json' );
        })

        // $(".fc-center .fc-button").on("click", function(e) {
        //     e.preventDefault();

        //     $(this).siblings().removeClass('active');
        //     $(this).addClass('active');
        // })
    }

});


// modal-city
$(document).ready(function() {
    if ($(".modal-city").length) {

        var div = document.createElement('div');

        div.style.overflowY = 'scroll';
        div.style.width =  '50px';
        div.style.height = '50px';
        div.style.visibility = 'hidden';
        document.body.appendChild(div);
        var scrollWidth = div.offsetWidth - div.clientWidth;
        document.body.removeChild(div);

        var
            trigerCity = $(".pageHeader__city-choose"),
            modalCity = $(".modal-city"),
            modalInner = modalCity.find(".modal-city__inner"),
            modalBody = modalCity.find(".modal-city__body"),
            trigerClose = modalCity.find(".modal-city__close");

        trigerClose.on("click", function(e) {
            e.preventDefault();
            document.body.style.overflow = "";
            modalCity.hide(0, function() {
                $(this).find(".modal-city__wrapper").hide();
            });
        });

        trigerCity.on("click", function(e) {
            e.preventDefault();
            document.body.style.overflow = "hidden";
            modalCity.show(0, function() {
                $(this).find(".modal-city__wrapper").fadeIn(300);
            });
            _setOverflow();
        });

        $(window).on("resize", function() {
            // console.log($(window).width());
            _setOverflow();
        });

        function _setOverflow() {

            if (modalInner.innerHeight() > modalBody.innerHeight()) {
                modalBody.removeClass("modal-city__body--no-fix");
                modalInner.removeClass("modal-city__inner--overflow-y");
                modalInner.css({"right": "-50px"});
            }

            if (modalInner.innerHeight() < modalBody.innerHeight() && !modalInner.hasClass('modal-city__inner--overflow-y')) {
                modalBody.addClass("modal-city__body--no-fix");
                modalInner.addClass("modal-city__inner--overflow-y");
                modalInner.css({"right": -(50 + scrollWidth) + "px"});
            } else {
                return;
            }
        }
    }
});

// modal-tel
$(document).ready(function() {
    if ($(".modal-tel").length) {

        var div = document.createElement('div');

        div.style.overflowY = 'scroll';
        div.style.width =  '50px';
        div.style.height = '50px';
        div.style.visibility = 'hidden';
        document.body.appendChild(div);
        var scrollWidth = div.offsetWidth - div.clientWidth;
        document.body.removeChild(div);

        var
            trigger = $(".pageHeader__contact-callback"),
            modal = $(".modal-tel"),
            modalInner = modal.find(".modal-tel__inner"),
            modalBody = modal.find(".modal-tel__body"),
            trigerClose = modal.find(".modal-tel__close"),
            form = modal.find("form"),
            inputs = form.find("input"),
            button = form.find("button");

        trigerClose.on("click", function(e) {
            e.preventDefault();
            //document.body.style.overflow = "";
            modal.fadeOut(100);
            _clearInputs();
        });

        trigger.on("click", function(e) {
            e.preventDefault();
            //document.body.style.overflow = "hidden";
            modal.fadeIn(100);
            if (_validation()) {
                button.removeAttr('disabled');
            } else {
                button.attr("disabled", "disabled");
            }
            _setOverflow();
        });

        $(window).on("resize", function() {
            // console.log($(window).width());
            _setOverflow();
        });

        function _setOverflow() {

            if (modalInner.innerHeight() > modalBody.innerHeight()) {
                modalBody.removeClass("modal-tel__body--no-fix");
                modalInner.removeClass("modal-tel__inner--overflow-y");
                modalInner.css({"right": "-50px"});
            }

            if (modalInner.innerHeight() < modalBody.innerHeight() && !modalInner.hasClass('modal-city__inner--overflow-y')) {
                modalBody.addClass("modal-tel__body--no-fix");
                modalInner.addClass("modal-tel__inner--overflow-y");
                modalInner.css({"right": -(50 + scrollWidth) + "px"});
            } else {
                return;
            }
        }

        inputs.each(function(i, elem) {
            $(elem).on("change", function() {

                if (_validation()) {
                    button.removeAttr('disabled');
                } else {
                    button.attr("disabled", "disabled");
                }
            });
        });

        function _validation() {

            var isValid = true;

            inputs.each(function (i, item) {
              var
                $input = $(item),
                val = $.trim($input.val());

              if (!val) {
                isValid = false;
              }

            });

            return isValid;
        }

        function _clearInputs() {
            form.trigger("reset");
        }
    }
});

// autocomplete small search
$(document).ready(function() {

    if (!$(".small-search__input").length) {return false;}

    console.log("search")

    var input = $(".small-search__input");

    var div = document.createElement('div');

        div.style.overflowY = 'scroll';
        div.style.width =  '50px';
        div.style.height = '50px';
        div.style.visibility = 'hidden';
        document.body.appendChild(div);
        var scrollWidth = div.offsetWidth - div.clientWidth;
        document.body.removeChild(div);

    function _setOverflow() {
        var
            modalInner = $(".modal-city__inner"),
            modalBody = $(".modal-city__body");

        if (modalInner.innerHeight() > modalBody.innerHeight()) {
            modalBody.removeClass("modal-city__body--no-fix");
            modalInner.removeClass("modal-city__inner--overflow-y");
            modalInner.css({"right": "-50px"});
        }

        if (modalInner.innerHeight() < modalBody.innerHeight() && !modalInner.hasClass('modal-city__inner--overflow-y')) {
            modalBody.addClass("modal-city__body--no-fix");
            modalInner.addClass("modal-city__inner--overflow-y");
            modalInner.css({"right": -(50 + scrollWidth) + "px"});
        } else {
            return;
        }
    }

    input.each(function(i, elem) {
        $(elem).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "city.json",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function(data) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        var array = $.grep(data, function (value) {
                            return matcher.test(value.label) || matcher.test(value.value);
                        });
                        response(array);
                    },
                    eror: function() {
                        console.log("fail");
                    }
                });
            },
            appendTo: ".small-search",
            autoFocus: true,
            delay: 300,
            minLength: 0,
            open: function( event, ui ) {
                // console.log(event);
                // console.log(ui);
                _setOverflow();
            },
            close: function( event, ui ) {
                $(".ui-autocomplete").css({"display": "block"});
                $(".ui-autocomplete").slideUp( "slow", function() {
                    //console.log("close");
                    _setOverflow();
                });
            }
        });

        $(window).on("resize", function() {
            $(elem).parent().find(".ui-autocomplete").css({"width": $(elem).parent().width()});
        });
    });
});

// popover
$(document).ready(function() {

    if ($('.popover').length) {
        var
        popover = $('.popover'),
        options = {
            content:'Content',
            closeable: true,
            animation:'pop',
            placement:'horizontal',
            // dismissible: false,
            // offsetLeft: 3,
            // offsetTop: 5,
            onShow: function(element) {
                // console.log(element);
            }
        };

        popover.webuiPopover(options);

        $(window).on("resize", function() {
            popover.webuiPopover('hide');
        });
    }
});

// modal-enter
// $(document).ready(function() {
//     if ($(".modal-enter").length) {

//         var div = document.createElement('div');

//         div.style.overflowY = 'scroll';
//         div.style.width =  '50px';
//         div.style.height = '50px';
//         div.style.visibility = 'hidden';
//         document.body.appendChild(div);
//         var scrollWidth = div.offsetWidth - div.clientWidth;
//         document.body.removeChild(div);

//         var
//             trigger = $(".pageHeader__auth-link"),
//             modal = $(".modal-enter"),
//             modalInner = modal.find(".modal-enter__inner"),
//             modalBody = modal.find(".modal-enter__body"),
//             trigerClose = modal.find(".modal-enter__close"),
//             form = modal.find("form"),
//             inputs = form.find("input").not("input[type='checkbox']"),
//             button = form.find("button"),
//             btnReg = modal.find(".modal-email__reg");

//         trigerClose.on("click", function(e) {
//             e.preventDefault();
//             //document.body.style.overflow = "";
//             modal.fadeOut(100);
//             _clearInputs();
//         });

//         trigger.on("click", function(e) {
//             e.preventDefault();
//             //document.body.style.overflow = "hidden";
//             modal.fadeIn(100);
//             if (_validation()) {
//                 button.removeAttr('disabled');
//             } else {
//                 button.attr("disabled", "disabled");
//             }
//             _setOverflow();
//         });

//         $(window).on("resize", function() {
//             // console.log($(window).width());
//             _setOverflow();
//         });

//         function _setOverflow() {

//             if (modalInner.innerHeight() > modalBody.innerHeight()) {
//                 modalBody.removeClass("modal-enter__body--no-fix");
//                 modalInner.removeClass("modal-enter__inner--overflow-y");
//                 modalInner.css({"right": "-50px"});
//             }

//             if (modalInner.innerHeight() < modalBody.innerHeight() && !modalInner.hasClass('modal-city__inner--overflow-y')) {
//                 modalBody.addClass("modal-enter__body--no-fix");
//                 modalInner.addClass("modal-enter__inner--overflow-y");
//                 modalInner.css({"right": -(50 + scrollWidth) + "px"});
//             } else {
//                 return;
//             }
//         }

//         inputs.each(function(i, elem) {
//             $(elem).on("change", function() {

//                 if (_validation()) {
//                     button.removeAttr('disabled');
//                 } else {
//                     button.attr("disabled", "disabled");
//                 }
//             });
//         });

//         function _validation() {

//             var isValid = true;

//             inputs.each(function (i, item) {

//               var
//                 $input = $(item),
//                 val = $.trim($input.val());

//               if (!val) {
//                 isValid = false;
//               }

//             });

//             // console.log(isValid);

//             return isValid;
//         }

//         function _clearInputs() {
//             form.trigger("reset");
//         }

//         // form.on("submit", function(e) {
//         //     e.preventDefault();
//         //     var data = form.serialize();
//         //     console.log($(this).serialize());
//         // });
//     }
// });

// modal-reg
// $(document).ready(function() {
//     if ($(".modal-reg").length) {

//         var div = document.createElement('div');

//         div.style.overflowY = 'scroll';
//         div.style.width =  '50px';
//         div.style.height = '50px';
//         div.style.visibility = 'hidden';
//         document.body.appendChild(div);
//         var scrollWidth = div.offsetWidth - div.clientWidth;
//         document.body.removeChild(div);

//         var
//             // trigger = $(".pageHeader__auth-link"),
//             modal = $(".modal-reg"),
//             modalInner = modal.find(".modal-reg__inner"),
//             modalBody = modal.find(".modal-reg__body"),
//             trigerClose = modal.find(".modal-reg__close"),
//             form = modal.find("form"),
//             inputs = form.find("input").not("input[type='checkbox']"),
//             button = form.find("button"),
//             btnReg = $(".regitstration");


//         trigerClose.on("click", function(e) {
//             e.preventDefault();
//             //document.body.style.overflow = "";
//             modal.fadeOut(100);
//             _clearInputs();
//         });

//         btnReg.on("click", function(e) {
//             e.preventDefault();
//             // console.log("click")
//             $(".modal-enter").fadeOut(100, function() {
//                 modal.fadeIn(100);
//                 if (_validation()) {
//                 button.removeAttr('disabled');
//             } else {
//                 button.attr("disabled", "disabled");
//             }
//             _setOverflow();
//             });
//         });

//         // trigger.on("click", function(e) {
//         //     e.preventDefault();
//         //     //document.body.style.overflow = "hidden";
//         //     modal.show();
//         //     if (_validation()) {
//         //         button.removeAttr('disabled');
//         //     } else {
//         //         button.attr("disabled", "disabled");
//         //     }
//         //     _setOverflow();
//         // });

//         $(window).on("resize", function() {
//             // console.log($(window).width());
//             _setOverflow();
//         });

//         function _setOverflow() {

//             if (modalInner.innerHeight() > modalBody.innerHeight()) {
//                 modalBody.removeClass("modal-reg__body--no-fix");
//                 modalInner.removeClass("modal-reg__inner--overflow-y");
//                 modalInner.css({"right": "-50px"});
//             }

//             if (modalInner.innerHeight() < modalBody.innerHeight() && !modalInner.hasClass('modal-city__inner--overflow-y')) {
//                 modalBody.addClass("modal-reg__body--no-fix");
//                 modalInner.addClass("modal-reg__inner--overflow-y");
//                 modalInner.css({"right": -(50 + scrollWidth) + "px"});
//             } else {
//                 return;
//             }
//         }

//         inputs.each(function(i, elem) {
//             $(elem).on("change", function() {

//                 if (_validation()) {
//                     button.removeAttr('disabled');
//                 } else {
//                     button.attr("disabled", "disabled");
//                 }
//             });
//         });

//         function _validation() {

//             var isValid = true;

//             inputs.each(function (i, item) {

//               var
//                 $input = $(item),
//                 val = $.trim($input.val());

//               if (!val) {
//                 isValid = false;
//               }

//             });

//             // console.log(isValid);

//             return isValid;
//         }

//         function _clearInputs() {
//             form.trigger("reset");
//         }

//         // form.on("submit", function(e) {
//         //     e.preventDefault();
//         //     var data = form.serialize();
//         //     console.log($(this).serialize());
//         // });
//     }
// });

// modal-email
// $(document).ready(function() {
//     if ($(".modal-email").length) {

//         var div = document.createElement('div');

//         div.style.overflowY = 'scroll';
//         div.style.width =  '50px';
//         div.style.height = '50px';
//         div.style.visibility = 'hidden';
//         document.body.appendChild(div);
//         var scrollWidth = div.offsetWidth - div.clientWidth;
//         document.body.removeChild(div);

//         var
//             // trigger = $(".pageHeader__auth-link"),
//             modal = $(".modal-email"),
//             modalInner = modal.find(".modal-email__inner"),
//             modalBody = modal.find(".modal-email__body"),
//             trigerClose = modal.find(".modal-email__close"),
//             form = modal.find("form"),
//             inputs = form.find("input").not("input[type='checkbox']"),
//             button = form.find("button"),
//             btnEmail = $(".fogot-pass");


//         trigerClose.on("click", function(e) {
//             e.preventDefault();
//             //document.body.style.overflow = "";
//             modal.fadeOut(100);
//             _clearInputs();
//         });

//         btnEmail.on("click", function(e) {
//             e.preventDefault();
//             // console.log("click")
//             $(".modal-enter").fadeOut(100, function() {
//                 modal.fadeIn(100);
//                 if (_validation()) {
//                 button.removeAttr('disabled');
//             } else {
//                 button.attr("disabled", "disabled");
//             }
//             _setOverflow();
//             });
//         });

//         // trigger.on("click", function(e) {
//         //     e.preventDefault();
//         //     //document.body.style.overflow = "hidden";
//         //     modal.show();
//         //     if (_validation()) {
//         //         button.removeAttr('disabled');
//         //     } else {
//         //         button.attr("disabled", "disabled");
//         //     }
//         //     _setOverflow();
//         // });

//         $(window).on("resize", function() {
//             // console.log($(window).width());
//             _setOverflow();
//         });

//         function _setOverflow() {

//             if (modalInner.innerHeight() > modalBody.innerHeight()) {
//                 modalBody.removeClass("modal-email__body--no-fix");
//                 modalInner.removeClass("modal-email__inner--overflow-y");
//                 modalInner.css({"right": "-50px"});
//             }

//             if (modalInner.innerHeight() < modalBody.innerHeight() && !modalInner.hasClass('modal-city__inner--overflow-y')) {
//                 modalBody.addClass("modal-email__body--no-fix");
//                 modalInner.addClass("modal-email__inner--overflow-y");
//                 modalInner.css({"right": -(50 + scrollWidth) + "px"});
//             } else {
//                 return;
//             }
//         }

//         inputs.each(function(i, elem) {
//             $(elem).on("change", function() {

//                 if (_validation()) {
//                     button.removeAttr('disabled');
//                 } else {
//                     button.attr("disabled", "disabled");
//                 }
//             });
//         });

//         function _validation() {

//             var isValid = true;

//             inputs.each(function (i, item) {

//               var
//                 $input = $(item),
//                 val = $.trim($input.val());

//               if (!val) {
//                 isValid = false;
//               }

//             });

//             // console.log(isValid);

//             return isValid;
//         }

//         function _clearInputs() {
//             form.trigger("reset");
//         }

//         // form.on("submit", function(e) {
//         //     e.preventDefault();
//         //     var data = form.serialize();
//         //     console.log($(this).serialize());
//         // });
//     }
// });

// messsage
$(document).ready(function() {
    if (!$('.message').length) {return false;}
    $('.message').each(function(){
        var $tab = $(this),
            $btn = $tab.find('.message__title');
        $btn.on('click', function(){
            $tab.toggleClass('open');
        });
    });
});

// camp-map
$(document).ready(function() {
    if ($("#map").length) {

        var
            mapSmall = $("#map"),
            dataLat = mapSmall.attr("data-lat"),
            dataLng = mapSmall.attr("data-lng"),
            imageClip = mapSmall.attr("data-image-clip").split(" "),
            centerArr = [+dataLat, +dataLng],
            smallMap,
            bigMap,
            imagePosition = [],
            showMap = $(".camp-card__map-show"),
            closeMap = $(".camp-card__map-close"),
            overlay = $(".camp-card__map-overlay");

        for (var k = 0; k < imageClip.length; k = k + 2) {
          var temp = [];

          temp.push(+imageClip[k]);
          ((imageClip[k + 1] != undefined) ? temp.push(+imageClip[k + 1]) : null);
          imagePosition.push(temp);
        }

        ymaps.ready(initSmall);

        function initSmall() {
            smallMap = new ymaps.Map("map", {
                center: centerArr,
                zoom: 15,
                controls: []
            }),
            myPlacemark = new ymaps.Placemark(smallMap.getCenter(), {
                hintContent: "Собственный значок метки"
            }, {
                iconLayout: "default#image",
                iconImageClipRect: imagePosition,
                iconImageHref: "img/map-pins.png",
                iconImageSize: [77, 65],
                iconImageOffset: [-27, -60]
            });
            smallMap.behaviors.disable(['scrollZoom', 'dblClickZoom', 'leftMouseButtonMagnifier', 'rightMouseButtonMagnifier', 'multiTouch']);
            smallMap.geoObjects.add(myPlacemark);
        }

        function initBig() {
            bigMap = new ymaps.Map("map-big", {
                center: centerArr,
                zoom: 15,
                controls: []
            }),
            myPlacemark = new ymaps.Placemark(bigMap.getCenter(), {
                hintContent: "Собственный значок метки"
            }, {
                iconLayout: "default#image",
                iconImageClipRect: imagePosition,
                iconImageHref: "img/map-pins.png",
                iconImageSize: [77, 65],
                iconImageOffset: [-27, -60]
            });
            bigMap.geoObjects.add(myPlacemark);
        }

        showMap.on("click", function (e) {
            e.preventDefault();
            var $this = $(this);

            // overlay.addClass("camp-card__map-overlay--visible");
            $(".camp-card__map-wrap").children(".camp-card__map-big").empty();
            overlay.fadeIn(100);
            ymaps.ready(initBig);
            $(".camp-card__map-wrap").fadeIn(100);
            // $(".camp-card__map-wrap").addClass("show-mod");
            // $(".camp-card__map-wrap").removeClass("hide-mod");
        });
        closeMap.on("click", function (e) {
            e.preventDefault();
            var $this = $(this);

            // overlay.removeClass("camp-card__map-overlay--visible");
            overlay.fadeOut(100);
            $(".camp-card__map-wrap").fadeOut(100);
            // $(".camp-card__map-wrap").removeClass("show-mod");
            // $(".camp-card__map-wrap").addClass("hide-mod");
        });
        overlay.on("click", function (e) {
            e.preventDefault();
            var $this = $(this);

            // overlay.removeClass("camp-card__map-overlay--visible");
            overlay.fadeOut(100);
            $(".camp-card__map-wrap").fadeOut(100);
            // $(".camp-card__map-wrap").removeClass("show-mod");
            // $(".camp-card__map-wrap").addClass("hide-mod");
        });
    }
});

$(document).ready(function() {
    if (!$(".programs-type__method").length) return false;
    var
        programsMethod = $(".programs-type__method"),
        programsMethodChildrens = programsMethod.children().not($(".programs-type__method-inner")),
        programsMethodInner = programsMethod.find(".programs-type__method-inner");

        function getSumsHeight(arr) {
            var newArr = 0;

            arr.each(function(i, item) {
                newArr += ($(item).outerHeight(true));
            });

            return newArr;
        }

        programsMethodInner.css({"height": programsMethod.parent().height() - +programsMethod.css("padding-bottom").slice(0, programsMethod.css("padding-bottom").length - 2) - +programsMethod.css("padding-top").slice(0, programsMethod.css("padding-top").length - 2) - getSumsHeight(programsMethodChildrens)});
});

// new map index page
$(document).ready(function() {
    if (!$("#map-section").length) return;

    var myMap;
    var item = {
        hint: "Палаточный лагерь в Костромской области вторая строка названия",
        address: {
            icon: "map-pin",
            title: "Россия, Сочи, курорт \“Сочинский\” Россия, Сочи, курорт \“Сочинский\” Россия, Сочи, курорт \“Сочинский\”"
        },
        // image: "img/baloon-img.jpg",
        image: "http://img.mehovaya-metelica.com/179/003/img-9380-3-150@.jpg",
        // image: "img/temporary/camp_01@2x.jpg",
        content: "<p>В 2014 году разработана Концепция развития Международного детского центра «Артек» – «Артек 2.0. Перезагрузка», цель которой – превратить детский центр в лучшую международную площадку по созданию, апробации и внедрению инновационных форм.234 В 2014 году разработана Концепция развития Международного детского центра «Артек» – «Артек 2.0. Перезагрузка», цель которой – превратить детский центр в лучшую международную площадку по созданию, апробации и внедрению инновационных форм.234</p><p>В 2014 году разработана Концепция развития Международного детского центра «Артек» – «Артек 2.0. Перезагрузка», цель которой – превратить детский центр в лучшую международную площадку по созданию, апробации и внедрению инновационных форм.234 В 2014 году разработана Концепция развития Международного детского центра «Артек» – «Артек 2.0. Перезагрузка», цель которой – превратить детский центр в лучшую международную площадку по созданию, апробации и внедрению инновационных форм.234</p>",
        programm: [
            {
                title: "Спортивная. Конный спорт. \"Гусарская баллада\"",
                age: "6-14"
            },
            {
                title: "Развивающая. Самостоятельность. \"Путь меча:Штурм крепости\"",
                age: "6-14"
            },
            {
                title: "Развивающая. Общее развитие. \"Погранзастава\"",
                age: "6-14"
            }
        ]
    };

    var baloonMarkup =
            "<div class='map-ballon'>"
                + "<div class='map-ballon__adderss'>"
                    + "<i data-ico='" + item.address.icon + "' class='ico ico--xs map-ballon__adderss-icon'>"
                        + "<svg class='ico__svg'>"
                            + "<use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#map-pin'></use>"
                        + "</svg></i>"
                    + "<span class='map-ballon__adderss-title'>" + item.address.title + "</span>"
                + "</div>"
                + "<a href='#' class='map-ballon__title'>" + item.hint + "</a>"
                + "<div class='map-ballon__desc'>"
                    + "<div class='map-ballon__desc-img-wrap'>"
                        + "<img class='map-ballon__desc-img' src='" +  item.image+ "'>"
                    + "</div>"
                    + "<div class='map-ballon__desc-text-wrap'>"
                        + "<div class='map-ballon__desc-text'>"  + item.content + "</div>"
                    + "</div>"
                + "</div>"
                 + "<div class='map-ballon__table'>"
                    + "<div class='map-ballon__table-head'>"
                        + "<div class='map-ballon__table-head-cell map-ballon__table-head-cell--left'>Программы</div>"
                        + "<div class='map-ballon__table-head-cell map-ballon__table-head-cell--right'>Возраст (лет)</div>"
                    + "</div>"
                    + "<div class='map-ballon__table-content'>"
                        + "<a href='#' class='map-ballon__table-content-cell  map-ballon__table-content-cell--left'>" + item.programm[0].title+ "</a>"
                        + "<div class='map-ballon__table-content-cell  map-ballon__table-content-cell--right'>" + item.programm[0].age + "</div>"
                    + "</div>"
                    + "<div class='map-ballon__table-content'>"
                        + "<a href='#' class='map-ballon__table-content-cell  map-ballon__table-content-cell--left'>" + item.programm[1].title+ "</a>"
                        + "<div class='map-ballon__table-content-cell  map-ballon__table-content-cell--right'>" + item.programm[1].age + "</div>"
                    + "</div>"
                    + "<div class='map-ballon__table-content'>"
                        + "<a href='#' class='map-ballon__table-content-cell  map-ballon__table-content-cell--left'>" + item.programm[2].title+ "</a>"
                        + "<div class='map-ballon__table-content-cell  map-ballon__table-content-cell--right'>" + item.programm[2].age + "</div>"
                    + "</div>"
                 + "</div>"
            + "</div>";

    ymaps.ready(init);

    function init(){
        myMap = new ymaps.Map("map-section", {
            center: [55.76, 37.64],
            zoom: 7
        });

        // Создаем шаблон для отображения контента балуна
        var balloonContentLayout = ymaps.templateLayoutFactory.createClass(baloonMarkup);

        // Помещаем созданный шаблон в хранилище шаблонов.
        ymaps.layout.storage.add('my#balloonContentLayout', balloonContentLayout);

        marker = new ymaps.Placemark([55.76, 37.64], {
            hintContent: item.hint,
        }, {
            balloonContentLayout: "my#balloonContentLayout",
            balloonMaxWidth: 420,
            // panelMaxHeightRatio: 0
        });

        myMap.geoObjects.add(marker);

        // myMap.events.add('balloonopen', function (e) {
        //     console.log("open");
        // });

        myMap.balloon.events
            .add('open', function () {
                var img = $(".map-ballon__desc-img");

                if (img.prop('complete')) {
                    // console.log('img from cach');
                    addStyle(img);
                    $("body").addClass('baloon-visible');
                } else {
                    img.load(function(){
                        // console.log($(this).prop('complete') + " new img loaded");
                        addStyle(img);
                        $("body").addClass('baloon-visible');
                    });
                }
            })
            .add('close', function () {
                console.log("close");
                $("body").removeClass('baloon-visible');
                $("[data-baloon='css-baloon']").remove();
            });
    }

    function getElemHeight(elem) {
        return elem.height();
    }

    function addStyle(elem) {
        var style = '<style type="text/css" data-baloon="css-baloon">.map-ballon__desc-text {height: ' + getElemHeight(elem) + 'px;}</style>';
        $(document.body).append(style);
    }
});


// programm shedule open
$(document).ready(function() {
  $(".scheduled-programm-title").on("click", function(e){
    var idSelector = $(this).attr("data-programScheduleID");
    $(".scheduled-programm-title").removeClass("scheduled-programm-title--active");
    $(this).toggleClass("scheduled-programm-title--active");
    $(".programm-schedule").removeClass("visible");
    $("#"+idSelector).toggleClass("visible");
    e.preventDefault();
  });
});

// camp transfer table open
$(document).ready(function() {

  $(".avail-camps__table-transfer").on("click", function(e){
    var idSelector = $(this).attr("data-campTransferID");
    // $(".transfer-wrap").toggleClass("hovered");
    $(this).parent().toggleClass("hovered");
    $("#"+idSelector).toggleClass("visible");
    e.preventDefault();
  });

  $(".camp-transfer-table__close").on("click", function(e){
    $(this).parent().removeClass("visible");
    $(".transfer-wrap").removeClass("hovered");
    e.preventDefault();
  });

});
